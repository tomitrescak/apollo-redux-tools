export { default as ApolloProvider } from './client/apollo_layout';

export { mantraRedux, createApp } from './client/redux';

export { connect } from 'react-apollo';
