import { Meteor } from 'meteor/meteor';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { registerGqlTag } from 'apollo-client/gql';
import { Accounts } from 'meteor/accounts-base';
var networkInterface = createNetworkInterface('/graphql');
networkInterface.use([{
        applyMiddleware: function (request, next) {
            var currentUserToken = Accounts._storedLoginToken();
            if (!currentUserToken) {
                next();
                return;
            }
            if (!request.options.headers) {
                request.options.headers = new Headers();
            }
            request.options.headers.Authorization = currentUserToken;
            next();
        }
    }]);
// init apollo
export var apolloClient = new ApolloClient({ networkInterface: networkInterface });
Meteor.startup(function () {
    registerGqlTag();
});
