import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { apolloClient } from './apollo';
import { mantraRedux } from './redux';
var Root = function (_a) {
    var children = _a.children;
    return (<ApolloProvider client={apolloClient} store={mantraRedux.store}>
    {children}
  </ApolloProvider>);
};
export default Root;
