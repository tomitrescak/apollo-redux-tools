// mantra
import { createApp as createMantraApp } from 'mantra-core';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import createLogger from 'redux-logger';
import ReduxThunk from 'redux-thunk';
import { apolloClient } from './apollo';
// extended mantra class
var MantraRedux = (function () {
    function MantraRedux() {
    }
    MantraRedux.prototype.assignContext = function (context) {
        this.app = createMantraApp(context);
        this.modules = [];
        this.context = context;
    };
    MantraRedux.prototype.loadModule = function (moduleDefinition) {
        this.app.loadModule(moduleDefinition);
        // rememeber reducers
        if (moduleDefinition.reducers) {
            this.modules.push(moduleDefinition);
        }
    };
    MantraRedux.prototype.init = function () {
        this.app.init();
        // prepare middlewares
        var middleware = [apolloClient.middleware(), ReduxThunk];
        if (process.env.NODE_ENV === 'development' && !window['devToolsExtension']) {
            var logger = createLogger();
            middleware.push(logger);
        }
        // prepare all reducers
        var reducers = { apollo: apolloClient.reducer() };
        for (var _i = 0, _a = this.modules; _i < _a.length; _i++) {
            var mantraModule = _a[_i];
            reducers = Object.assign(reducers, mantraModule.reducers);
        }
        this.store = createStore(combineReducers(reducers), {}, compose(applyMiddleware.apply(void 0, middleware), window['devToolsExtension'] ? window['devToolsExtension']() : function (f) { return f; }));
        this.context.Store = this.store;
        // if (module['hot']) {
        //     // Enable Webpack hot module replacement for reducers
        //     module['hot'].accept('../reducers', () => {
        //       const nextReducer = require('../reducers').default;
        //       store.replaceReducer(nextReducer);
        //     });
        //   }
    };
    return MantraRedux;
}());
export var mantraRedux = new MantraRedux(); // this instance is used for accessing the store
export var createApp = function (context) {
    mantraRedux.assignContext(context);
    return mantraRedux;
};
