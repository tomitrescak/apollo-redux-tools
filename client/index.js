export { default as ApolloProvider } from './apollo_layout';
export { mantraRedux, createApp } from './redux';
export { connect } from 'react-apollo';
