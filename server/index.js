import {
  apolloServer
} from 'graphql-tools';
import express from 'express';
import proxyMiddleware from 'http-proxy-middleware';

import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';

const graphQLServer = express();
const resolverBuilder = {};

const Posts = new Mongo.Collection("po");

const rb = {
  Query: {
    async posts() {
      return Posts.find({}).fetch();
    },
    async post(root: any, { id }: any) {
      return Posts.findOne(id);
    },
    async comments(root: any, { postId }: any) {
      return Comments.find({ postId }).fetch();
    }
  },
  Mutation: {
    async addPost(root: any, { title, content }: any) {
      const postId = Posts.insert({ title, content });
      return postId;
    },
    async removePost(root: any, { id }: any) {
      Posts.remove(id);
      return true;
    },
    async addComment(root: any, { postId, comment }: any, context: any) {
      const id = Comments.insert({ postId: postId, text: comment, createdAt: new Date().getTime(), author: context.user._id });
      return Posts.findOne(id);
    }
  }
}

export function initServer({ schema, resolvers = rb, port = 4000 }) {
  graphQLServer.use('/graphql', apolloServer(async(req) => {
    let user = null;
    // if (req.headers.authorization) {
    //   const token = req.headers.authorization;
    //   check(token, String);
    //   const hashedToken = Accounts._hashLoginToken(token);
    //   user = await Meteor.users.findOne({
    //     "services.resume.loginTokens.hashedToken": hashedToken
    //   });
    // }

    return {
      graphiql: true,
      pretty: true,
      schema,
      resolvers,
      context: {
        // The current user will now be available on context.user in all resolvers
        user,
      }
    };
  }));

  graphQLServer.listen(port, () => console.log(
    `GraphQL Server is now running on http://localhost:${port}`
  ));

  WebApp.rawConnectHandlers.use(proxyMiddleware(`http://localhost:${port}/graphql`));
}


// this allows us to incrementally build resolvers and to avoid the async keyword

export function addQueries(fcs) {
  addResolvers("Query", fcs);
}

export function addMutations(fcm) {
  addResolvers("Mutation", fcm);
}

export function addResolvers(key, obj) {
  if (!resolverBuilder[key]) {
    resolverBuilder[key] = {};
  }

  for (let resolverKey in obj) {
    if (resolverBuilder[key][resolverKey]) {
      throw 'Resolver allready contains: ' + resolverKey;
    }

    resolverBuilder[key][resolverKey] = async (root, args) => {
      return obj[resolverKey].call(this, root, args);
    }
  }
}

export function addAsyncResolvers(key, obj) {
  if (!resolverBuilder[key]) {
    resolverBuilder[key] = {};
  }

  for (let resolverKey in obj) {
    if (resolverBuilder[key][resolverKey]) {
      throw 'Resolver allready contains: ' + resolverKey;
    }
    resolverBuilder[key][resolverKey] = obj[resolverKey];
  }
}
